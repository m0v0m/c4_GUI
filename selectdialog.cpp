﻿#include "selectdialog.h"
#include "ui_selectdialog.h"

SelectDialog::SelectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelectDialog)
{
    ui->setupUi(this);
    setWindowTitle("Channel selection");

    ui->LeftChButton->setText("Shutdown");
    ui->RightChButton->setText("Reboot");
    ui->TableChButton->setText("Quit");
}

SelectDialog::~SelectDialog()
{
    delete ui;
}

void SelectDialog::on_LeftChButton_clicked()
{
   system("sudo halt");
   reject();
}

void SelectDialog::on_RightChButton_clicked()
{
    system("sudo reboot");
    reject();
}

void SelectDialog::on_TableChButton_clicked()
{
    QCoreApplication::quit();
    reject();
}
