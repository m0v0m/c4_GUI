﻿#include "dialog.h"
#include "ui_dialog.h"
#include <QPainter>

#include "selectdialog.h"
//聲道顯示名稱
QString showchannel_L;
QString showchannel_R;
QString showchannel_T;
//預被寫入顯示的字串
QString str_L;
QString str_R;
QString str_T;
//預備寫入的通道編號
int useChennal_L;
int useChennal_R;
int useChennal_T;
//啦叭靜音前的狀態
int PrefL1vol;
int PrefR1vol;
int PrefM1vol;
//通道名稱總表
QString testlab[30];
//語音通訊裝置文字狀態總表
QString StatusStr[5];
//通訊管制表
char ContrlTable[54][30];
//按鍵代表之通訊通道編號
int ChennalOfMagephoneBtn[3];
int ChennalOfShipInBtn[9];
int ChennalOfShipOutBtn[18];
//按鍵上的通道名稱
QString NameOfMagephoneBtn[3];
QString NameOfShipInBtn[9];
QString NameOfShipOutBtn[18];// 艦外通道名稱固定不給編輯
//按鍵自身編號
int MarkBtn_L;
int MarkBtn_R;
int MarkBtn_T;
//外部觸發GPIO腳位_@piOnly
int ExButten;
//螢幕虛PTT擬按鈕狀態
bool virtualPTTbtn;
//封包異動旗標
bool bufDiff;
//回應通訊教官台之批次旗標
int bufEcho;
//管制麥克風權限
bool micL_Authority;
bool micR_Authority;
bool micT_Authority;
//系統是否參照通訊管制表自動介入
bool AutoImportCtrl;




Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{    
    ui->setupUi(this);

    setWindowTitle("wentestSM_w");
    //GPIO初始化及腳位功能定義_@piOnly
    setenv("WIRINGPI_GPIOMEM","1",1);//取消wiringPiSetup()的權限警告,使其程式不會中斷執行
    wiringPiSetup();
    ExButten = 0;
    pinMode(ExButten,INPUT);
    //初始虛擬按鈕狀態
    virtualPTTbtn = false;
    //智文a網路模組第一版
    HostIP = "192.168.180.99";
    LocalIP = getIpAdress();
    //LocalIP = "192.168.180.20"; //only for test 上系統時由上一行指令取得該硬體IP
    qDebug() << LocalIP;
    stand_ID=LocalIP.right(2);
    //將各位數的十位數塞0
    (stand_ID[0] == '.') ? stand_ID[0] = '0':stand_ID[0] =stand_ID[0] ;
    istand_ID=stand_ID.toInt();
    ui->systemButten->setText(stand_ID);
    portout = 1112;
    portin = 1111;
    udpSocket = new QUdpSocket(this);
    connect(udpSocket,SIGNAL(readyRead()),this,SLOT(dataReceived()));
   udpSocket->bind(portin);
   //隱藏測試用的站台輸入功能
   ui->label->hide();
   ui->ClientNumber->hide();
   //ui->systemButten->hide();
    //智文a網路模組第一版_END

    //將文字檔words.txt寫入指定變數tastlab[28]
    QFile dictionaryFile("/home/pi/Sea4/Reference/words.txt");
    dictionaryFile.open(QFile::ReadOnly);
    QTextStream inputStream(&dictionaryFile);

    int i =0;
    while (!inputStream.atEnd()) {
        QString word;
        inputStream >> word;
        if (!word.isEmpty()) {
            testlab[i]=word;
            i++;
        }
    }
    //將文字檔words.txt寫入指定變數tastlab[28]_END

    //賦予GUI各按鍵編號
    configUiPtr();
    checkedColor = "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 rgb(249, 178, 147), stop: 1 rgb(239, 147, 108));border-style: solid;border-width: 1px;border-radius: 5px;border-color: rgb(167, 167, 167) ;";
    uncheckedColor = "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 rgb(254, 254, 254), stop: 1 rgb(236, 236, 236));border-style: solid;border-width: 1px;border-radius: 5px;border-color: rgb(167, 167, 167) ;";

    //將各按鍵顯示外部文字檔的設定名稱
    for (int i=0;i<3;i++) NameOfMagephoneBtn[i] = testlab[i];
    for (int i=0;i<9;i++) NameOfShipInBtn[i] = testlab[i+3];
    for (int i=0;i<18;i++) NameOfShipOutBtn[i] = testlab[i+12];

    //更新按鍵上的文字
    updateBtnText();

    //設定一個週期處發的時間事件
    QTimer *timer = new QTimer(this);
    //关联定时器溢出信号和相应的槽函数
    connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
    timer->start(500);//更新速度1000為一秒鍾
    //設定一個週期處發的時間事件_END

    //語音裝備標籤字內容及格式顏色定義
    //將文字檔status.txt寫入指定變數StatusStr[4]
    QFile dictionaryFile_2("/home/pi/Sea4/Reference/status.txt");
    dictionaryFile_2.open(QFile::ReadOnly);
    QTextStream inputStream_2(&dictionaryFile_2);

    int j =0;
    while (!inputStream_2.atEnd()) {
        QString word;
        inputStream_2 >> word;
        if (!word.isEmpty()) {
            StatusStr[j]=word;
            j++;
        }
    }
    //將文字檔status.txt寫入指定變數StatusStr[4]_END
    //設置文字內容
    SpeakerEnableStr = StatusStr[0];
    SpeakerDisableStr = StatusStr[1];
    MicEnableStr= StatusStr[2];
    MicDisableStr = StatusStr[3];
    MicRestrictionStr = StatusStr[4];
    //設置文字為粗體
    EnableFont.setBold(true);
    DisableFont.setBold(true);
    //設置文字大小
    EnableFont.setPointSize(23);
    DisableFont.setPointSize(23);
    //設置文字顏色
    EnableColor.setColor(QPalette::WindowText,"#2a8f2a");
    DisableColor.setColor(QPalette::WindowText,Qt::red); //Qt::red 同 "#dd2222"
    RestrictionColor.setColor(QPalette::WindowText,Qt::gray);

    //麥克風、喇叭圖片顯示
    speakerL_stauts = true;
    speakerR_stauts = true;
    speakerT_stauts = true;
    micL_stauts = true;
    sendData.bL1mic = micL_stauts;
    micR_stauts = true;
    sendData.bR1mic = micR_stauts;
    micT_stauts = true;
    sendData.bM1mic = micT_stauts;
    PPT_stauts = false;
    sendData.bPTT=PPT_stauts;

    //音量初始化
    sendData.fL1vol =80;
    sendData.fR1vol =80;
    sendData.fM1vol=80;

    //預設按鍵
    MarkBtn_L = 0;
    MarkBtn_R = 0;
    MarkBtn_T = 0;

    //刷新各區通訊裝態旗標
    channelL_stauts = 1;
    channelR_stauts = 1;
    channelT_stauts = 1;

    //各區通道初始狀態顯示
    str_L = "No Signal";
    str_R = "No Signal";
    str_T = "No Signal";

    //站台ID之後用IP寫入
    sendData.iStandNo = istand_ID;

    //初始化封包異動旗標
    bufDiff = false;

    //系統是否參照通訊管制表自動介入 true:yes false:no
    AutoImportCtrl = true;

}

Dialog::~Dialog()
{
    delete ui;
}

//智文a網路模組第一版
void Dialog::dataReceived()
{
    while(udpSocket->hasPendingDatagrams())
    {
        udpSocket->readDatagram((char*)buf_from_net, sizeof (buf_from_net));
    }
    //判斷是'$'的系統命令封包,或'@'的通道名稱封包，還是'#'的管制表資料封包
    if(buf_from_net[0]=='$')
    {
        CMD_FROM_HOST *pDataIn = NULL;
        pDataIn = (CMD_FROM_HOST*) buf_from_net;
        CopyMemory( &(recvCMD), &(pDataIn->CpInCMD), sizeof(CPIN_CMD) );
        //解析系統操作命令
        ExecuteSystemCommand();
    }

    if(buf_from_net[0]=='@')
    {
        Name_FROM_HOST *pDataIn = NULL;
        pDataIn = (Name_FROM_HOST*) buf_from_net;
        CopyMemory( &(recvName), &(pDataIn->CpInName), sizeof(CPIN_Name) );
        //解析封包建立通道名稱表
        configChannelName();
    }

    if(buf_from_net[0]=='#')
    {
        DATA_FROM_HOST *pDataIn = NULL;
        pDataIn = (DATA_FROM_HOST*) buf_from_net;
        CopyMemory( &(recvData), &(pDataIn->CpInDate), sizeof(CPIN_Date) );
        //建立通訊管制表
        configContrlTable();
        //刷新各區通訊裝態旗標
        channelL_stauts = 1;
        channelR_stauts = 1;
        channelT_stauts = 1;
    }
}
//智文a網路模組第一版_END

//取得IP位址
QString Dialog::getIpAdress()
{
    //取得網卡資訊
    QList<QNetworkInterface> list = QNetworkInterface::allInterfaces();
         foreach(QNetworkInterface interface,list)
         {
             qDebug() <<"Device:"<<interface.name();
             qDebug() << "HardwareAddress:"<<interface.hardwareAddress();


             QList<QNetworkAddressEntry> entryList = interface.addressEntries();
          //取得IP位址清單,並包含一個IP位址,一個子遮罩及一個廣播位址
             foreach(QNetworkAddressEntry entry,entryList)
             {
                 qDebug()<<"IP Address:"<<entry.ip().toString();
                 qDebug()<<"Netmask: "  <<entry.netmask().toString();
                 qDebug()<<"Broadcast:" <<entry.broadcast().toString();
                 //請先用ifconfig確認網卡是eth0還是eth1或其他
                 if(interface.name() == "eth0") return entry.ip().toString() ;
             }
     }
         return 0;
}
//取得IP位址_END

//更新按鍵上的文字
void Dialog::updateBtnText()
{
    //廣播系統
    ui->MagephoneButton_01->setText(NameOfMagephoneBtn[0]);
    ui->MagephoneButton_02->setText(NameOfMagephoneBtn[1]);
    ui->MagephoneButton_03->setText(NameOfMagephoneBtn[2]);
    //鑑內通訊
    ui->ShipInButton_01->setText(NameOfShipInBtn[0]);
    ui->ShipInButton_02->setText(NameOfShipInBtn[1]);
    ui->ShipInButton_03->setText(NameOfShipInBtn[2]);
    ui->ShipInButton_04->setText(NameOfShipInBtn[3]);
    ui->ShipInButton_05->setText(NameOfShipInBtn[4]);
    ui->ShipInButton_06->setText(NameOfShipInBtn[5]);
    ui->ShipInButton_07->setText(NameOfShipInBtn[6]);
    ui->ShipInButton_08->setText(NameOfShipInBtn[7]);
    ui->ShipInButton_09->setText(NameOfShipInBtn[8]);
    //鑑外通訊
    ui->ShipOutButton_01->setText(NameOfShipOutBtn[0]);
    ui->ShipOutButton_02->setText(NameOfShipOutBtn[1]);
    ui->ShipOutButton_03->setText(NameOfShipOutBtn[2]);
    ui->ShipOutButton_04->setText(NameOfShipOutBtn[3]);
    ui->ShipOutButton_05->setText(NameOfShipOutBtn[4]);
    ui->ShipOutButton_06->setText(NameOfShipOutBtn[5]);
    ui->ShipOutButton_07->setText(NameOfShipOutBtn[6]);
    ui->ShipOutButton_08->setText(NameOfShipOutBtn[7]);
    ui->ShipOutButton_09->setText(NameOfShipOutBtn[8]);
    ui->ShipOutButton_10->setText(NameOfShipOutBtn[9]);
    ui->ShipOutButton_11->setText(NameOfShipOutBtn[10]);
    ui->ShipOutButton_12->setText(NameOfShipOutBtn[11]);
    ui->ShipOutButton_13->setText(NameOfShipOutBtn[12]);
    ui->ShipOutButton_14->setText(NameOfShipOutBtn[13]);
    ui->ShipOutButton_15->setText(NameOfShipOutBtn[14]);
    ui->ShipOutButton_16->setText(NameOfShipOutBtn[15]);
    ui->ShipOutButton_17->setText(NameOfShipOutBtn[16]);
    ui->ShipOutButton_18->setText(NameOfShipOutBtn[17]);
    //將各按鍵顯示外部文字檔的設定名稱_END
}

//解析系統操作命令
void Dialog::ExecuteSystemCommand()
{
     //收到字元執行對應命令1.A艦關機2.B艦關機3.多一關機4.多二關機5.多三關機7.程式關閉8.重新開機9.系統關機
    if ((recvCMD.CommandOfHost == '1') && ((istand_ID > 0) && (istand_ID <= 20))) system("sudo halt");
    else if ((recvCMD.CommandOfHost == '2') && ((istand_ID > 20) && (istand_ID <= 40))) system("sudo halt");
    else if ((recvCMD.CommandOfHost == '3') && ((istand_ID > 40) && (istand_ID <= 45))) system("sudo halt");
    else if ((recvCMD.CommandOfHost == '4') && ((istand_ID > 45) && (istand_ID <= 50))) system("sudo halt");
    else if ((recvCMD.CommandOfHost == '5') && ((istand_ID > 50) && (istand_ID <= 55))) system("sudo halt");
    else if (recvCMD.CommandOfHost == '7') QCoreApplication::quit();
    else if (recvCMD.CommandOfHost == '8') system("sudo reboot");
    else if (recvCMD.CommandOfHost == '9') system("sudo halt");
}

//解析封包建立通道名稱表
void Dialog::configChannelName()
{
    QString AllShipInName;
    AllShipInName = QString(recvName.NameOfShipInChannel);

    QString ShipAInChannel[12];
    QString ShipBInChannel[12];
    QString ExOneInChannel[2];
    QString ExTwoInChannel[2];
    QString ExThreeInChannel[2];
    // 將收到的整包字串分類寫入AB艦中
    for(int i=0;i<12;i++)
    {
        ShipAInChannel[i] = AllShipInName.section(';',i,i);
        ShipBInChannel[i] = AllShipInName.section(';',i+12,i+12);
        //testlab[i] = (istand_ID < 20) ? ShipAInChannel[i] : ShipAInChannel[i];
        if ((istand_ID > 0) && (istand_ID <= 20))  {testlab[i] = ShipAInChannel[i]; bufEcho = 1;}
        else if ((istand_ID > 20) && (istand_ID <= 40))  {testlab[i] = ShipBInChannel[i]; bufEcho = 2;}
    }
    // 將收到的整包字串分類寫入三間多功，能室且多功能室只提供兩組艦內通訊頻道
    for(int i=0;i<2;i++)
    {
        ExOneInChannel[i] = AllShipInName.section(';',i+24,i+24);
        ExTwoInChannel[i] = AllShipInName.section(';',i+26,i+26);
        ExThreeInChannel[i] = AllShipInName.section(';',i+28,i+28);
        if ((istand_ID > 40) && (istand_ID <= 45))  {testlab[i+3] = ExOneInChannel[i]; bufEcho = 3;}
        else if ((istand_ID > 45) && (istand_ID <= 50))  {testlab[i+3] = ExTwoInChannel[i]; bufEcho = 4;}
        else if ((istand_ID > 50) && (istand_ID <= 55))  {testlab[i+3] = ExThreeInChannel[i]; bufEcho = 5;}
    }
    //將各按鍵顯示外部文字檔的設定名稱
    for (int i=0;i<3;i++) NameOfMagephoneBtn[i] = testlab[i];
    for (int i=0;i<9;i++) NameOfShipInBtn[i] = testlab[i+3];
    for (int i=0;i<18;i++) NameOfShipOutBtn[i] = testlab[i+12];
    //更新新收到的通道名稱
    updateBtnText();
}

//解析封包建立通訊管制表
 void Dialog::configContrlTable()
 {
     //通訊管制表ContrlTable清空
     for (int i=0;i<54;i++)
     {
         for (int j=0;j<30;j++)
         {
           ContrlTable[i][j]='0';
         }
     }
     //通訊管制表單艦寫入ContrlTable
     for (int i=0;i<19;i++)
     {
         for (int j=0;j<30;j++)
         {
           ContrlTable[i][j]=recvData.RegelationofShipA[i*30+j];
           ContrlTable[i+20][j]=recvData.RegelationofShipB[i*30+j];
         }
     }
     //通訊管制表多功能室寫入ContrlTable
     for (int i=0;i<4;i++)
     {
         for (int j=0;j<2;j++)
         {
           ContrlTable[i+40][j+3]=recvData.RegelationofExOne[i*20+j];
           ContrlTable[i+45][j+3]=recvData.RegelationofExTwo[i*20+j];
           ContrlTable[i+50][j+3]=recvData.RegelationofExThree[i*20+j];
         }
         for (int k=0;k<18;k++)
         {
           ContrlTable[i+40][k+12]=recvData.RegelationofExOne[i*20+k+2];
           ContrlTable[i+45][k+12]=recvData.RegelationofExTwo[i*20+k+2];
           ContrlTable[i+50][k+12]=recvData.RegelationofExThree[i*20+k+2];
         }
     }
 }
//解析封包建立通訊管制表_END

 //更新通訊管制表設定
 void Dialog::updateContrlTable()
{
     //only for test
//     ContrlTable[istand_ID-1][0]='1';
//     ContrlTable[istand_ID-1][1]='0';
//     ContrlTable[istand_ID-1][2]='2';

//     ContrlTable[istand_ID-1][3]='1';
//     ContrlTable[istand_ID-1][4]='0';
//     ContrlTable[istand_ID-1][5]='2';
//     ContrlTable[istand_ID-1][6]='1';
//     ContrlTable[istand_ID-1][7]='1';
//     ContrlTable[istand_ID-1][8]='2';
//     ContrlTable[istand_ID-1][9]='1';
//     ContrlTable[istand_ID-1][10]='1';
//     ContrlTable[istand_ID-1][11]='2';

//     ContrlTable[istand_ID-1][12]='1';
//     ContrlTable[istand_ID-1][13]='0';
//     ContrlTable[istand_ID-1][14]='1';
//     ContrlTable[istand_ID-1][15]='1';
//     ContrlTable[istand_ID-1][16]='1';
//     ContrlTable[istand_ID-1][17]='1';
//     ContrlTable[istand_ID-1][18]='2';
//     ContrlTable[istand_ID-1][19]='2';
//     ContrlTable[istand_ID-1][20]='2';
//     ContrlTable[istand_ID-1][21]='2';
//     ContrlTable[istand_ID-1][22]='2';
//     ContrlTable[istand_ID-1][23]='2';
//     ContrlTable[istand_ID-1][24]='1';
//     ContrlTable[istand_ID-1][25]='1';
//     ContrlTable[istand_ID-1][26]='1';
//     ContrlTable[istand_ID-1][27]='2';
//     ContrlTable[istand_ID-1][28]='2';
//     ContrlTable[istand_ID-1][29]='2';
     //only for test_END

     //按鍵上的通道設置
     //通道設置清空
     for (int i=0;i<3;i++) {ChennalOfMagephoneBtn[i] = 999; MagephoneBtnText[i] = "color:red;";}
     for (int i=0;i<9;i++) {ChennalOfShipInBtn[i] = 999; ShipInBtnText[i] = "color:red;";}
     for (int i=0;i<18;i++) {ChennalOfShipOutBtn[i] = 999; ShipOutBtnText[i]  = "color:red;";}

     int j;
     j = 0;
     for(int i=0;i<3;i++)
     {
         if(ContrlTable[istand_ID-1][i] != '0')
         {
             //寫入該按鍵所對應的通道編號，字體顏色，權限
             ChennalOfMagephoneBtn[j] = (istand_ID < 20) ? (501+i) : (513+i);
             MagephoneBtnText[j] = (ContrlTable[istand_ID-1][i] == '1') ?  "color:blue;" : "color:black;" ;
             MagephoneBtnAuthority[j] = (ContrlTable[istand_ID-1][i] == '1') ?  1 : 2 ;
             //寫入該按鍵須顯示的通道名稱
             NameOfMagephoneBtn[j] = testlab[i] ;//testlab[]已再configChannelName()時判斷過該名單是A艦還是B艦了
              j++;
         }
     }

     j = 0;
     for(int i=0;i<9;i++)
     {
         if(ContrlTable[istand_ID-1][i+3] != '0')
         {
             //寫入該按鍵所對應的通道編號，字體顏色，權限
             if (istand_ID <= 20)   ChennalOfShipInBtn[j] = 504+i;
             else if((istand_ID >= 21) && (istand_ID <= 40))    ChennalOfShipInBtn[j] = 516+i;
             else if((istand_ID >= 41) && (istand_ID <= 45))    ChennalOfShipInBtn[j] = 543+i;
             else if((istand_ID >= 46) && (istand_ID <= 50))    ChennalOfShipInBtn[j] = 545+i;
             else if((istand_ID >= 51) && (istand_ID <= 55))    ChennalOfShipInBtn[j] = 547+i;
             ShipInBtnText[j]  = (ContrlTable[istand_ID-1][i+3] == '1') ?  "color:blue;" : "color:black;" ;
             ShipInBtnAuthority[j] = (ContrlTable[istand_ID-1][i+3] == '1') ?  1 : 2 ;
             //寫入該按鍵須顯示的通道名稱
             NameOfShipInBtn[j] = testlab[i+3] ;//testlab[]已再configChannelName()時判斷過該名單是A艦還是B艦了
             j++;
         }
     }

     j = 0;
     for(int i=0;i<18;i++)
     {
         if(ContrlTable[istand_ID-1][i+12] != '0')
         {
             //寫入該按鍵所對應的通道編號，字體顏色，權限
             ChennalOfShipOutBtn[j]=525+i;
              ShipOutBtnText[j]  = (ContrlTable[istand_ID-1][i+12] == '1') ?  "color:blue;" : "color:black;" ;
              ShipOutBtnAuthority[j] = (ContrlTable[istand_ID-1][i+12] == '1') ?  1 : 2 ;
              //寫入該按鍵須顯示的通道名稱
              NameOfShipOutBtn[j] = testlab[i+12] ;// 艦外通道名稱固定不給編輯
              j++;
         }
     }
     //更新按鍵的顯示名稱
     updateBtnText();
 }

 //隱藏沒有用到的通道按鈕
void Dialog::HideDisableBtn()
{
    //艦內廣播部份
    ChennalOfMagephoneBtn[0] == 999 ? ui->MagephoneButton_01->hide() : ui->MagephoneButton_01->show();
    ChennalOfMagephoneBtn[1] == 999 ? ui->MagephoneButton_02->hide() : ui->MagephoneButton_02->show();
    ChennalOfMagephoneBtn[2] == 999 ? ui->MagephoneButton_03->hide() : ui->MagephoneButton_03->show();
    //艦內通道部份
    ChennalOfShipInBtn[0] == 999 ? ui->ShipInButton_01->hide() : ui->ShipInButton_01->show();
    ChennalOfShipInBtn[1] == 999 ? ui->ShipInButton_02->hide() : ui->ShipInButton_02->show();
    ChennalOfShipInBtn[2] == 999 ? ui->ShipInButton_03->hide() : ui->ShipInButton_03->show();
    ChennalOfShipInBtn[3] == 999 ? ui->ShipInButton_04->hide() : ui->ShipInButton_04->show();
    ChennalOfShipInBtn[4] == 999 ? ui->ShipInButton_05->hide() : ui->ShipInButton_05->show();
    ChennalOfShipInBtn[5] == 999 ? ui->ShipInButton_06->hide() : ui->ShipInButton_06->show();
    ChennalOfShipInBtn[6] == 999 ? ui->ShipInButton_07->hide() : ui->ShipInButton_07->show();
    ChennalOfShipInBtn[7] == 999 ? ui->ShipInButton_08->hide() : ui->ShipInButton_08->show();
    ChennalOfShipInBtn[8] == 999 ? ui->ShipInButton_09->hide() : ui->ShipInButton_09->show();
    //艦外通道部份
    ChennalOfShipOutBtn[0] == 999 ? ui->ShipOutButton_01->hide() : ui->ShipOutButton_01->show();
    ChennalOfShipOutBtn[1] == 999 ? ui->ShipOutButton_02->hide() : ui->ShipOutButton_02->show();
    ChennalOfShipOutBtn[2] == 999 ? ui->ShipOutButton_03->hide() : ui->ShipOutButton_03->show();
    ChennalOfShipOutBtn[3] == 999 ? ui->ShipOutButton_04->hide() : ui->ShipOutButton_04->show();
    ChennalOfShipOutBtn[4] == 999 ? ui->ShipOutButton_05->hide() : ui->ShipOutButton_05->show();
    ChennalOfShipOutBtn[5] == 999 ? ui->ShipOutButton_06->hide() : ui->ShipOutButton_06->show();
    ChennalOfShipOutBtn[6] == 999 ? ui->ShipOutButton_07->hide() : ui->ShipOutButton_07->show();
    ChennalOfShipOutBtn[7] == 999 ? ui->ShipOutButton_08->hide() : ui->ShipOutButton_08->show();
    ChennalOfShipOutBtn[8] == 999 ? ui->ShipOutButton_09->hide() : ui->ShipOutButton_09->show();
    ChennalOfShipOutBtn[9] == 999 ? ui->ShipOutButton_10->hide() : ui->ShipOutButton_10->show();
    ChennalOfShipOutBtn[10] == 999 ? ui->ShipOutButton_11->hide() : ui->ShipOutButton_11->show();
    ChennalOfShipOutBtn[11] == 999 ? ui->ShipOutButton_12->hide() : ui->ShipOutButton_12->show();
    ChennalOfShipOutBtn[12] == 999 ? ui->ShipOutButton_13->hide() : ui->ShipOutButton_13->show();
    ChennalOfShipOutBtn[13] == 999 ? ui->ShipOutButton_14->hide() : ui->ShipOutButton_14->show();
    ChennalOfShipOutBtn[14] == 999 ? ui->ShipOutButton_15->hide() : ui->ShipOutButton_15->show();
    ChennalOfShipOutBtn[15] == 999 ? ui->ShipOutButton_16->hide() : ui->ShipOutButton_16->show();
    ChennalOfShipOutBtn[16] == 999 ? ui->ShipOutButton_17->hide() : ui->ShipOutButton_17->show();
    ChennalOfShipOutBtn[17] == 999 ? ui->ShipOutButton_18->hide() : ui->ShipOutButton_18->show();
}

//隱藏多功能室的廣播系統
void Dialog::HideExRoomMagephoneSysteam()
{
    if( istand_ID > 40){
        ui->groupBox_3->hide();
        ui->TableLabel->hide();
        ui->MagephoneButton_01->hide() ;
        ui->MagephoneButton_02->hide() ;
        ui->MagephoneButton_03->hide() ;
        ui->T_mic_status->hide();
        ui->T_mic_switch->hide() ;
        ui->T_speaker_status->hide();
        ui->T_speaker_switch->hide() ;
        ui->label_8->hide() ;
        ui->TableSlider->hide();
    }
}

//將管制通道內的使用者踢出
void Dialog::RemoveFromSystem()
{
    //踢出通道旗標
    bool RemoveFlag ;
    //判斷廣播禁用通道內有無使用者
    RemoveFlag = true;
    for(int x=1;x<4;x++)
    {
        if(ChennalOfMagephoneBtn[x-1] == useChennal_T) RemoveFlag = false;
    }
    if(RemoveFlag)
    {
        useChennal_T = 999;
        str_T = "No Signal";
        MarkBtn_T = 0;
    }
    //判斷艦內通訊禁用通道內有無使用者
    RemoveFlag = true;
    for(int x=4;x<13;x++)
    {
        if(ChennalOfShipInBtn[x-4] == useChennal_L) RemoveFlag = false;
    }
    if(RemoveFlag)
    {
        useChennal_L = 999;
        str_L = "No Signal";
        MarkBtn_L = 0;
    }
    //判斷艦外通訊禁用通道內有無使用者
    RemoveFlag = true;
    for(int x=13;x<31;x++)
    {
        if(ChennalOfShipOutBtn[x-13] == useChennal_R) RemoveFlag = false;
    }
    if(RemoveFlag)
    {
        useChennal_R = 999;
        str_R = "No Signal";
        MarkBtn_R = 0;
    }
}

//按鍵提示色動態調整
void Dialog::MarkShift()
{
    //判斷廣播通道有無須變更色塊位子
    for(int x=1;x<4;x++)
    {
        if((ChennalOfMagephoneBtn[x-1] == useChennal_T) && (useChennal_T != 999)) MarkBtn_T = x;
    }
    //判斷艦內通訊通道有無須變更色塊位子
    for(int x=4;x<13;x++)
    {
        if((ChennalOfShipInBtn[x-4] == useChennal_L) && (useChennal_L != 999)) MarkBtn_L = x;
    }
    //判斷艦外通訊通道有無須變更色塊位子
    for(int x=13;x<31;x++)
    {
        if((ChennalOfShipOutBtn[x-13] == useChennal_R) && (useChennal_R != 999)) MarkBtn_R = x;
    }
}

void Dialog::timerUpdate()
{
    //更新通訊管制計畫
    updateContrlTable();
     //隱藏沒用到的通道
    HideDisableBtn();
    //隱藏多功能室的廣播系統
    HideExRoomMagephoneSysteam();
    //將管制通道內的使用者踢出
    RemoveFromSystem();
    //按鍵提示色動態調整
    MarkShift();
    //將最新設定的通道名稱寫入顯示GUI上
    showchannel_L = str_L;
    showchannel_R = str_R;
    showchannel_T = str_T;
    if (channelL_stauts==1){
        ui->LeftLabel->setText(showchannel_L);
        sendData.iL1ch = useChennal_L;
        //艦內通訊按鍵上色,麥克風權限判斷
        micL_Authority = true;
        for(int x=4;x<13;x++)
        {
            if(MarkBtn_L==x) ShipInBtnPtr[x-4]->setStyleSheet(checkedColor+ ShipInBtnText[x-4]);
            else  ShipInBtnPtr[x-4]->setStyleSheet(uncheckedColor+ShipInBtnText[x-4]);

            if((MarkBtn_L==x) && (ShipInBtnAuthority[x-4] == 1) && AutoImportCtrl) micL_Authority = false;
        }
    }
    if (channelR_stauts==1){
        ui->RightLabel->setText(showchannel_R);
        sendData.iR1ch = useChennal_R;
        //艦外通訊按鍵上色,麥克風權限判斷
        micR_Authority = true;
        for(int x=13;x<31;x++)
        {
            if(MarkBtn_R==x) ShipOutBtnPtr[x-13]->setStyleSheet(checkedColor+ShipOutBtnText[x-13]);
            else  ShipOutBtnPtr[x-13]->setStyleSheet(uncheckedColor+ShipOutBtnText[x-13]);

            if((MarkBtn_R==x) && (ShipOutBtnAuthority[x-13] == 1) && AutoImportCtrl) micR_Authority = false;
        }
    }
    if (channelT_stauts==1){
        ui->TableLabel->setText(showchannel_T);
        sendData.iM1ch = useChennal_T;
        //廣播區域按鍵上色,麥克風權限判斷
        micT_Authority = true;
        for(int x=1;x<4;x++)
        {
            if(MarkBtn_T==x) MagephoneBtnPtr[x-1]->setStyleSheet(checkedColor+MagephoneBtnText[x-1]);
            else  MagephoneBtnPtr[x-1]->setStyleSheet(uncheckedColor+MagephoneBtnText[x-1]);

            if((MarkBtn_T==x) && (MagephoneBtnAuthority[x-1] == 1 ) && AutoImportCtrl) micT_Authority = false;
        }
    }
    //重製待寫入狀態,以防佔用到非屬於自身群組的顯示框
    channelL_stauts = 0;
    channelR_stauts = 0;
    channelT_stauts = 0;

    //將最新設定的通道名稱寫入顯示GUI上_END

    //載入外部圖片
    //喇叭
    QPixmap enableSP("/home/pi/Sea4/Reference/enableSP.png");
    QIcon enableSPicon(enableSP);
    QPixmap disableSP("/home/pi/Sea4/Reference/disableSP.png");
    QIcon disableSPicon(disableSP);
    //麥克風
    QPixmap enableMIC("/home/pi/Sea4/Reference/enableMIC.png");
    QIcon enableMICicon(enableMIC);
    QPixmap disableMIC("/home/pi/Sea4/Reference/disableMIC.png");
    QIcon disableMICicon(disableMIC);
    //載入外部圖片_END

    //是否變更按鍵上的圖片
    //喇叭
    //耳機左聲道喇叭
    if (speakerL_stauts==1){
        enableSP = enableSP.scaled(52,52, Qt::KeepAspectRatio);
        ui->L_speaker_switch->setIcon(enableSPicon);
        ui->L_speaker_switch->setIconSize(enableSP.size());
        ui->L_speaker_status->setText(SpeakerEnableStr);//標籤字
        ui->L_speaker_status->setFont(EnableFont);
        ui->L_speaker_status->setPalette(EnableColor);
    }
    else{
        disableSP = disableSP.scaled(52,52, Qt::KeepAspectRatio);
        ui->L_speaker_switch->setIcon(disableSPicon);
        ui->L_speaker_switch->setIconSize(disableSP.size());
        ui->L_speaker_status->setText(SpeakerDisableStr);//標籤字
        ui->L_speaker_status->setFont(DisableFont);
        ui->L_speaker_status->setPalette(DisableColor);
    }
    //耳機右聲道喇叭
    if (speakerR_stauts==1){
        enableSP = enableSP.scaled(52,52, Qt::KeepAspectRatio);
        ui->R_speaker_switch->setIcon(enableSPicon);
        ui->R_speaker_switch->setIconSize(enableSP.size());
        ui->R_speaker_status->setText(SpeakerEnableStr);//標籤字
        ui->R_speaker_status->setFont(EnableFont);
        ui->R_speaker_status->setPalette(EnableColor);
    }
    else{
        disableSP = disableSP.scaled(52,52, Qt::KeepAspectRatio);
        ui->R_speaker_switch->setIcon(disableSPicon);
        ui->R_speaker_switch->setIconSize(disableSP.size());
        ui->R_speaker_status->setText(SpeakerDisableStr);//標籤字
        ui->R_speaker_status->setFont(DisableFont);
        ui->R_speaker_status->setPalette(DisableColor);
    }
    //桌面聲道喇叭
    if (speakerT_stauts==1){
        enableSP = enableSP.scaled(52,52, Qt::KeepAspectRatio);
        ui->T_speaker_switch->setIcon(enableSPicon);
        ui->T_speaker_switch->setIconSize(enableSP.size());
        ui->T_speaker_status->setText(SpeakerEnableStr);//標籤字
        ui->T_speaker_status->setFont(EnableFont);
        ui->T_speaker_status->setPalette(EnableColor);
    }
    else{
        disableSP = disableSP.scaled(52,52, Qt::KeepAspectRatio);
        ui->T_speaker_switch->setIcon(disableSPicon);
        ui->T_speaker_switch->setIconSize(disableSP.size());
        ui->T_speaker_status->setText(SpeakerDisableStr);//標籤字
        ui->T_speaker_status->setFont(DisableFont);
        ui->T_speaker_status->setPalette(DisableColor);
    }
    //麥克風
    //耳機左聲道麥克風
    if (micL_stauts==1){
        if(micL_Authority){
            enableMIC = enableMIC.scaled(52,52, Qt::KeepAspectRatio);
            ui->L_mic_switch->setIcon(enableMICicon);
            ui->L_mic_switch->setIconSize(enableMIC.size());
            ui->L_mic_switch->setEnabled(true);
            ui->L_mic_status->setText(MicEnableStr);//標籤字
            ui->L_mic_status->setFont(EnableFont);
            ui->L_mic_status->setPalette(EnableColor);
            sendData.bL1mic = true;
        }
        else{
            disableMIC = disableMIC.scaled(52,52, Qt::KeepAspectRatio);
            ui->L_mic_switch->setIcon(disableMICicon);
            ui->L_mic_switch->setIconSize(disableMIC.size());
            ui->L_mic_switch->setEnabled(false);
            ui->L_mic_status->setText(MicRestrictionStr);//標籤字
            ui->L_mic_status->setFont(DisableFont);
            ui->L_mic_status->setPalette(RestrictionColor);
            sendData.bL1mic = false;
        }
    }
    else{
        disableMIC = disableMIC.scaled(52,52, Qt::KeepAspectRatio);
        ui->L_mic_switch->setIcon(disableMICicon);
        ui->L_mic_switch->setIconSize(disableMIC.size());
        micL_Authority ? ui->L_mic_switch->setEnabled(true) : ui->L_mic_switch->setEnabled(false) ;
        micL_Authority ? ui->L_mic_status->setText(MicDisableStr) : ui->L_mic_status->setText(MicRestrictionStr);//標籤字
        ui->L_mic_status->setFont(DisableFont);
        micL_Authority ? ui->L_mic_status->setPalette(DisableColor) : ui->L_mic_status->setPalette(RestrictionColor);
        sendData.bL1mic = false;
    }
    //耳機右聲道麥克風
    if (micR_stauts==1){
        if(micR_Authority){
            enableMIC = enableMIC.scaled(52,52, Qt::KeepAspectRatio);
            ui->R_mic_switch->setIcon(enableMICicon);
            ui->R_mic_switch->setIconSize(enableMIC.size());
            ui->R_mic_switch->setEnabled(true);
            ui->R_mic_status->setText(MicEnableStr);//標籤字
            ui->R_mic_status->setFont(EnableFont);
            ui->R_mic_status->setPalette(EnableColor);\
            sendData.bR1mic = true;
        }
        else{
            disableMIC = disableMIC.scaled(52,52, Qt::KeepAspectRatio);
            ui->R_mic_switch->setIcon(disableMICicon);
            ui->R_mic_switch->setIconSize(disableMIC.size());
            ui->R_mic_switch->setEnabled(false);
            ui->R_mic_status->setText(MicRestrictionStr);//標籤字
            ui->R_mic_status->setFont(DisableFont);
            ui->R_mic_status->setPalette(DisableColor);
            ui->R_mic_status->setPalette(RestrictionColor);
            sendData.bR1mic = false;
        }
    }
    else{
        disableMIC = disableMIC.scaled(52,52, Qt::KeepAspectRatio);
        ui->R_mic_switch->setIcon(disableMICicon);
        ui->R_mic_switch->setIconSize(disableMIC.size());
        micR_Authority ? ui->R_mic_switch->setEnabled(true) :ui->R_mic_switch->setEnabled(false) ;
        micR_Authority ? ui->R_mic_status->setText(MicDisableStr) : ui->R_mic_status->setText(MicRestrictionStr);//標籤字
        ui->R_mic_status->setFont(DisableFont);
        micR_Authority ? ui->R_mic_status->setPalette(DisableColor) : ui->R_mic_status->setPalette(RestrictionColor);
        sendData.bR1mic = false;
    }
    //桌面聲道麥克風
    if (micT_stauts==1){
        if(micT_Authority){
            enableMIC = enableMIC.scaled(52,52, Qt::KeepAspectRatio);
            ui->T_mic_switch->setIcon(enableMICicon);
            ui->T_mic_switch->setIconSize(enableMIC.size());
            ui->T_mic_switch->setEnabled(true);
            ui->T_mic_status->setText(MicEnableStr);//標籤字
            ui->T_mic_status->setFont(EnableFont);
            ui->T_mic_status->setPalette(EnableColor);
            sendData.bM1mic = true;
        }
        else
        {
            disableMIC = disableMIC.scaled(52,52, Qt::KeepAspectRatio);
            ui->T_mic_switch->setIcon(disableMICicon);
            ui->T_mic_switch->setIconSize(disableMIC.size());
            ui->T_mic_switch->setEnabled(false);
            ui->T_mic_status->setText(MicRestrictionStr);//標籤字
            ui->T_mic_status->setFont(DisableFont);
            ui->T_mic_status->setPalette(DisableColor);
            ui->T_mic_status->setPalette(RestrictionColor);
            sendData.bM1mic = false;
        }
    }
    else{
        disableMIC = disableMIC.scaled(52,52, Qt::KeepAspectRatio);
        ui->T_mic_switch->setIcon(disableMICicon);
        ui->T_mic_switch->setIconSize(disableMIC.size());
        micT_Authority ? ui->T_mic_switch->setEnabled(true) :ui->T_mic_switch->setEnabled(false) ;
        micT_Authority ? ui->T_mic_status->setText(MicDisableStr) : ui->T_mic_status->setText(MicRestrictionStr);//標籤字
        ui->T_mic_status->setFont(DisableFont);
        micT_Authority ? ui->T_mic_status->setPalette(DisableColor) : ui->T_mic_status->setPalette(RestrictionColor);
        sendData.bM1mic = false;
    }
    //是否變更按鍵上的圖片_END

    //先判斷觸控螢幕的按鍵有無被按下以避免兩者搶資源_@piOnly
    if(!virtualPTTbtn){
       //讀取外部觸發GPIO腳位狀態_@piOnly
       sendData.bPTT = digitalRead(ExButten) == HIGH ? true : false;
    }
    //根據PTT的使用狀態切換按鍵顏色
    sendData.bPTT == true ? ui->PTTButton->setStyleSheet(checkedColor) : ui->PTTButton->setStyleSheet(uncheckedColor);

     //only for test 輸入站台ID_END
//    QString str;
//    bool ok;
//    str = ui->ClientNumber->text();
//    sendData.iStandNo= str.toInt(&ok);
     //輸入站台ID_END

    //智文a網路模組第一版
    ZeroMemory(buf_to_net, sizeof buf_to_net);
    CopyMemory(buf_to_net, &sendData, sizeof sendData);
    //判斷封包是否有異動
    for(int i=0;i<1024;i++)
    {
        if(buf_register[i] != buf_to_net[i])
        {
            bufDiff = true;
            break;
        }
        else
        {
            bufDiff = false;
        }
    }
    //封包有異動或被要求回覆才傳送新的狀態
    if((bufDiff == true) || (bufEcho == 1))
    {
        for(int i=0;i<1024;i++) buf_register[i] = buf_to_net[i];
        QHostAddress serverAddress = QHostAddress(HostIP);
        udpSocket->writeDatagram((char *)buf_to_net,sizeof(sendData),serverAddress,portout);//傳出網路封包的cmd
        bufEcho = 0;
    }
    //分批回覆依序為A艦B艦多1多2多3
    if (bufEcho!=0) bufEcho-=1;
    //智文a網路模組第一版_END

}


/***廣播系統按鍵說明***
void Dialog::on_ShipInButton_01_clicked()

//設定桌面喇叭固定為艦外通訊
//    channelL_stauts = 0;
//    channelR_stauts = 0;
    channelT_stauts = 1;
    str_T = testlab[0];
    useChennal_T = 101;

//第二層對話框（海軍建議一層搞定）
    //    SelectDialog *dlg= new SelectDialog(this);
    //    dlg->show();
}
***廣播系統按鍵說明END***/
void Dialog::on_MagephoneButton_01_clicked()
{
    channelT_stauts = 1;
    str_T = NameOfMagephoneBtn[0] ;
    useChennal_T = ChennalOfMagephoneBtn[0];
    MarkBtn_T = 1;
}

void Dialog::on_MagephoneButton_02_clicked()
{
    channelT_stauts = 1;
    str_T = NameOfMagephoneBtn[1];
    useChennal_T = ChennalOfMagephoneBtn[1];
    MarkBtn_T = 2;
}

void Dialog::on_MagephoneButton_03_clicked()
{
    channelT_stauts = 1;
    str_T = NameOfMagephoneBtn[2];
    useChennal_T = ChennalOfMagephoneBtn[2];
    MarkBtn_T = 3;
}
/***艦內通訊按鍵說明***
void Dialog::on_ShipInButton_01_clicked()

//設定桌面喇叭固定為艦內通訊
    channelL_stauts = 1;
    //channelR_stauts = 0;
    //channelT_stauts = 0;
    str_L = testlab[3];
    useChennal_L = 104;

//第二層對話框（海軍建議一層搞定）
    //    SelectDialog *dlg= new SelectDialog(this);
    //    dlg->show();
}
***艦內通訊按鍵說明END***/
void Dialog::on_ShipInButton_01_clicked()
{
    channelL_stauts = 1;
    str_L= NameOfShipInBtn[0];
    useChennal_L = ChennalOfShipInBtn[0] ;
    MarkBtn_L = 4;
}

void Dialog::on_ShipInButton_02_clicked()
{
    channelL_stauts = 1;
    str_L= NameOfShipInBtn[1];
    useChennal_L = ChennalOfShipInBtn[1];
    MarkBtn_L = 5;
}

void Dialog::on_ShipInButton_03_clicked()
{
    channelL_stauts = 1;
    str_L= NameOfShipInBtn[2];
    useChennal_L = ChennalOfShipInBtn[2];
    MarkBtn_L = 6;
}
void Dialog::on_ShipInButton_04_clicked()
{
    channelL_stauts = 1;
    str_L= NameOfShipInBtn[3];
    useChennal_L = ChennalOfShipInBtn[3];
    MarkBtn_L = 7;
}

void Dialog::on_ShipInButton_05_clicked()
{
    channelL_stauts = 1;
    str_L= NameOfShipInBtn[4];
    useChennal_L = ChennalOfShipInBtn[4];
    MarkBtn_L = 8;
}

void Dialog::on_ShipInButton_06_clicked()
{
    channelL_stauts = 1;
    str_L= NameOfShipInBtn[5];
    useChennal_L = ChennalOfShipInBtn[5];
    MarkBtn_L = 9;
}

void Dialog::on_ShipInButton_07_clicked()
{
    channelL_stauts = 1;
    str_L= NameOfShipInBtn[6];
    useChennal_L = ChennalOfShipInBtn[6];
    MarkBtn_L = 10;
}

void Dialog::on_ShipInButton_08_clicked()
{
    channelL_stauts = 1;
    str_L= NameOfShipInBtn[7];
    useChennal_L = ChennalOfShipInBtn[7];
    MarkBtn_L = 11;
}

void Dialog::on_ShipInButton_09_clicked()
{
    channelL_stauts = 1;
    str_L= NameOfShipInBtn[8];
    useChennal_L = ChennalOfShipInBtn[8];
    MarkBtn_L = 12;
}
/***艦外通訊按鍵說明***
void Dialog::on_ShipOutButton_01_clicked()
{
//設定右耳機固定為艦外通訊
//    channelL_stauts = 0;//艦內
       channelR_stauts = 1;//艦外
//    channelT_stauts = 0;//廣播

    str_R= testlab[16];//當下按鍵名稱
    useChennal_R = 201;//帳號名稱

//第二層對話框（海軍建議一層搞定）
    //    SelectDialog *dlg= new SelectDialog(this);
    //    dlg->show();
}
***艦外通訊按鍵說明END***/
void Dialog::on_ShipOutButton_01_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[0];
    useChennal_R = ChennalOfShipOutBtn[0];
    MarkBtn_R = 13;
}

void Dialog::on_ShipOutButton_02_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[1];
    useChennal_R = ChennalOfShipOutBtn[1];
    MarkBtn_R = 14;

}

void Dialog::on_ShipOutButton_03_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[2];
    useChennal_R = ChennalOfShipOutBtn[2];
    MarkBtn_R = 15;
}

void Dialog::on_ShipOutButton_04_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[3];
    useChennal_R = ChennalOfShipOutBtn[3];
    MarkBtn_R = 16;
}

void Dialog::on_ShipOutButton_05_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[4];
    useChennal_R = ChennalOfShipOutBtn[4];
    MarkBtn_R = 17;
}

void Dialog::on_ShipOutButton_06_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[5];
    useChennal_R = ChennalOfShipOutBtn[5];
    MarkBtn_R = 18;
}

void Dialog::on_ShipOutButton_07_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[6];
    useChennal_R = ChennalOfShipOutBtn[6];
    MarkBtn_R = 19;
}

void Dialog::on_ShipOutButton_08_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[7];
    useChennal_R = ChennalOfShipOutBtn[7];
    MarkBtn_R = 20;
}

void Dialog::on_ShipOutButton_09_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[8];
    useChennal_R = ChennalOfShipOutBtn[8];
    MarkBtn_R = 21;
}

void Dialog::on_ShipOutButton_10_clicked()
{
   channelR_stauts = 1;
    str_R= NameOfShipOutBtn[9];
    useChennal_R = ChennalOfShipOutBtn[9];
    MarkBtn_R = 22;
}

void Dialog::on_ShipOutButton_11_clicked()
{
   channelR_stauts = 1;
    str_R= NameOfShipOutBtn[10];
    useChennal_R = ChennalOfShipOutBtn[10];
    MarkBtn_R = 23;
}

void Dialog::on_ShipOutButton_12_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[11];
    useChennal_R = ChennalOfShipOutBtn[11];
    MarkBtn_R = 24;
}

void Dialog::on_ShipOutButton_13_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[12];
    useChennal_R = ChennalOfShipOutBtn[12];
    MarkBtn_R = 25;
}

void Dialog::on_ShipOutButton_14_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[13];
    useChennal_R = ChennalOfShipOutBtn[13];
    MarkBtn_R = 26;
}

void Dialog::on_ShipOutButton_15_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[14];
    useChennal_R = ChennalOfShipOutBtn[14];
    MarkBtn_R = 27;
}

void Dialog::on_ShipOutButton_16_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[15];
    useChennal_R = ChennalOfShipOutBtn[15];
    MarkBtn_R = 28;
}

void Dialog::on_ShipOutButton_17_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[16];
    useChennal_R = ChennalOfShipOutBtn[16];
    MarkBtn_R = 29;
}

void Dialog::on_ShipOutButton_18_clicked()
{
    channelR_stauts = 1;
    str_R= NameOfShipOutBtn[17];
    useChennal_R = ChennalOfShipOutBtn[17];
    MarkBtn_R = 30;
}

void Dialog::on_L_speaker_switch_clicked()
{
    speakerL_stauts = !speakerL_stauts;
    //紀錄按下靜音前的資訊
    PrefL1vol =  (speakerL_stauts) ?  PrefL1vol  :  sendData.fL1vol;
    sendData.fL1vol = (speakerL_stauts) ?  PrefL1vol  :  0;
    //紀錄按下靜音前的資訊_end
}

void Dialog::on_R_speaker_switch_clicked()
{
     speakerR_stauts = !speakerR_stauts;
     //紀錄按下靜音前的資訊
     PrefR1vol =  (speakerR_stauts) ?  PrefR1vol  :  sendData.fR1vol;
     sendData.fR1vol = (speakerR_stauts) ?  PrefR1vol  :  0;
     //紀錄按下靜音前的資訊_end
}

void Dialog::on_T_speaker_switch_clicked()
{
    speakerT_stauts = !speakerT_stauts;
    //紀錄按下靜音前的資訊
    PrefM1vol  =  (speakerT_stauts) ?  PrefM1vol  :  sendData.fM1vol;
    sendData.fM1vol = (speakerT_stauts) ?  PrefM1vol  :  0;
    //紀錄按下靜音前的資訊_end
}

void Dialog::on_L_mic_switch_clicked()
{
    micL_stauts = !micL_stauts;
}

void Dialog::on_R_mic_switch_clicked()
{
    micR_stauts = !micR_stauts;
}

void Dialog::on_T_mic_switch_clicked()
{
    micT_stauts = !micT_stauts;
}

void Dialog::on_PTTButton_clicked()
{
    PPT_stauts = !PPT_stauts;
}

void Dialog::on_L_HeadSlider_valueChanged(int value)
{
    sendData.fL1vol = value;
}

void Dialog::on_R_HeadSlider_valueChanged(int value)
{
    sendData.fR1vol = value;
}

void Dialog::on_TableSlider_valueChanged(int value)
{
    (speakerT_stauts)  ?   sendData.fM1vol =value: PrefM1vol =value;
}

void Dialog::on_PTTButton_pressed()
{
    sendData.bPTT = true;
    virtualPTTbtn = true;
}

void Dialog::on_PTTButton_released()
{
    sendData.bPTT = false;
    virtualPTTbtn = false;
}

void Dialog::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPixmap headPhonePix;
    headPhonePix.load("/home/pi/Sea4/Reference/Headphone.png");
    painter.translate(602,605);
    painter.drawPixmap(0, 0, 115, 115, headPhonePix);
    //多功能室不需要桌面喇叭,多功能室的IP>40
    if(istand_ID <= 40){
        QPixmap tableSpeakerPix;
        tableSpeakerPix.load("/home/pi/Sea4/Reference/TableSpeaker.png");
        painter.translate(260,0);
        painter.drawPixmap(0, 0, 115, 115,  tableSpeakerPix);
    }
}

void Dialog::configUiPtr()
{
    MagephoneBtnPtr[0] = ui->MagephoneButton_01;
    MagephoneBtnPtr[1] = ui->MagephoneButton_02;
    MagephoneBtnPtr[2] = ui->MagephoneButton_03;

    ShipInBtnPtr[0] = ui->ShipInButton_01;
    ShipInBtnPtr[1] = ui->ShipInButton_02;
    ShipInBtnPtr[2] = ui->ShipInButton_03;
    ShipInBtnPtr[3] = ui->ShipInButton_04;
    ShipInBtnPtr[4] = ui->ShipInButton_05;
    ShipInBtnPtr[5] = ui->ShipInButton_06;
    ShipInBtnPtr[6] = ui->ShipInButton_07;
    ShipInBtnPtr[7] = ui->ShipInButton_08;
    ShipInBtnPtr[8] = ui->ShipInButton_09;

    ShipOutBtnPtr[0] = ui->ShipOutButton_01;
    ShipOutBtnPtr[1] = ui->ShipOutButton_02;
    ShipOutBtnPtr[2] = ui->ShipOutButton_03;
    ShipOutBtnPtr[3] = ui->ShipOutButton_04;
    ShipOutBtnPtr[4] = ui->ShipOutButton_05;
    ShipOutBtnPtr[5] = ui->ShipOutButton_06;
    ShipOutBtnPtr[6] = ui->ShipOutButton_07;
    ShipOutBtnPtr[7] = ui->ShipOutButton_08;
    ShipOutBtnPtr[8] = ui->ShipOutButton_09;
    ShipOutBtnPtr[9] = ui->ShipOutButton_10;
    ShipOutBtnPtr[10] = ui->ShipOutButton_11;
    ShipOutBtnPtr[11] = ui->ShipOutButton_12;
    ShipOutBtnPtr[12] = ui->ShipOutButton_13;
    ShipOutBtnPtr[13] = ui->ShipOutButton_14;
    ShipOutBtnPtr[14] = ui->ShipOutButton_15;
    ShipOutBtnPtr[15] = ui->ShipOutButton_16;
    ShipOutBtnPtr[16] = ui->ShipOutButton_17;
    ShipOutBtnPtr[17] = ui->ShipOutButton_18;

}

void Dialog::on_systemButten_clicked()
{
    SelectDialog *dlg= new SelectDialog(this);
    dlg->show();
}
