﻿#ifndef GLOBAL
#define GLOBAL

//本機輸出的資料結構
#pragma pack(1) //要Compiler以1個byte的位元組對齊
typedef struct
{
    int iStandNo;   //站台ID
    int iL1ch;      //Left 1 channel selected number                     life channel_wen
    int iL2ch;      //Left 2 channel selected number
    int iL3ch;      //Left 3 channel selected number
    int iR1ch;      //Right 1 channel selected number                  right channel_wen
    int iR2ch;      //Right 2 channel selected number
    int iR3ch;      //Right 3 channel selected number
    int iM1ch;      //Middle 1 chansendData.bPTT = true;nel selected number             table channel_wen
    int iM2ch;      //Middle 2 channel selected number
    int iSpare1;
    float fL1vol;   //Left 1 channel volumn                                     0.0~1.0 life volumn_wen
    float fL2vol;   //Left 2 channel volumn
    float fL3vol;   //Left 3 channel volumn
    float fR1vol;   //Right 1 channel volumn                                  0.0~1.0 right volumn_wen
    float fR2vol;   //Right 2 channel volumn
    float fR3vol;   //Right 3 channel volumn
    float fM1vol;   //Middle 1 channel volumn                             0.0~1.0 table volumn_wen
    float fM2vol;   //Middle 2 channel volumn
    float fABCvol;  //IOS broadcast received volumn
    float fSpare2;
    quint8 bL1mic;    //Left 1 mic on/off                                             life chennel mic SW_wen
    quint8 bL2mic;    //Left 2 mic on/off
    quint8 bL3mic;    //Left 3 mic on/off
    quint8 bR1mic;    //Right 1 mic on/off                                          right chennel mic SW_wen
    quint8 bR2mic;    //Right 2 mic on/off
    quint8 bR3mic;    //Right 3 mic on/off
    quint8 bM1mic;    //Middle 1 mic on/off                                      table chennel mic SW_wen
    quint8 bM2mic;    //Middle 2 mic on/off
    quint8 bPTT;      //PTT switch on/off                                              PTT_BTU_wen
    quint8 bA1speaker;//IOS broadcast out to A1 speaker
    quint8 bA2speaker;//IOS broadcast out to A2 speaker
    quint8 bB1speaker;//IOS broadcast out to B1 speaker
    quint8 bB2speaker;//IOS broadcast out to B2 speaker
    quint8 bC1speaker;//IOS broadcast out to C1 speaker
    quint8 bC2speaker;//IOS broadcast out to C2 speaker
    quint8 bC3speaker;//IOS broadcast out to C3 speaker
    quint8 bSpare3;
}CPOUT;
#pragma pack()//取消位元組對齊如此可將100byte->97byte

//本機接收的資料結構
typedef struct{
    char CommandOfID;
    char CommandOfHost;
}CPIN_CMD;

typedef struct{
    char RegelationofID;
    //單艦:(廣播*3+艦內*9+艦外*18)*(操作台*19)＝570
    char RegelationofShipA[570];
    char RegelationofShipB[570];
    //多功能室:(廣播*2+艦外*18)*(操作台*4)＝80
    char RegelationofExOne[80];
    char RegelationofExTwo[80];
    char RegelationofExThree[80];
   // int command;
}CPIN_Date;

typedef struct{
    char NameOfID;
     //A艦:(廣播*3+艦內*9)+B艦:(廣播*3+艦內*9)+多1:(艦內*2)+多2:(艦內*2)+多2:(艦內*2)
    char NameOfShipInChannel[1024];
}CPIN_Name;

typedef struct {
    CPIN_CMD	CpInCMD;
} CMD_FROM_HOST;

typedef struct {
    CPIN_Date	CpInDate;
} DATA_FROM_HOST;

typedef struct {
    CPIN_Name	CpInName;
} Name_FROM_HOST;

#endif // GLOBAL

