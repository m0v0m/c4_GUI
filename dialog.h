﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "iomap.h"
#include <QTimer>
#include <QDateTime>
#include <QFile>
#include <QTextStream>
#include<QNetworkInterface>
#include <qprocess.h>
#include "wiringPi.h" //wiringPi的lib_@piOnly


//智文a網路模組第一版
#include "global.h"
#include <QUdpSocket>
#define RtlCopyMemory(Destination,Source,Length) memcpy((Destination),(Source),(Length))
#define RtlZeroMemory(Destination,Length) memset((Destination),0,(Length))
#define CopyMemory RtlCopyMemory
#define ZeroMemory RtlZeroMemory
//智文a網路模組第一版_END

//智文sharedMemory
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#define BUFSZ 2048
//智文sharedMemory_END

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    //智文a網路模組第一版
    void dataReceived();
    //取得IP位址
    QString getIpAdress();
    //更新按鍵上的文字
    void updateBtnText();
    //執行系統操作命令
    void ExecuteSystemCommand();
    //建立艦內通道名稱表
    void configChannelName();
    //建立通訊管制表
    void configContrlTable();
    //套用通訊管制表
    void updateContrlTable();
    //隱藏被關閉的通道
    void HideDisableBtn();
    //隱藏多功能室的廣播系統
    void HideExRoomMagephoneSysteam();
    //將管制通道內的使用者踢出
    void RemoveFromSystem();
    //按鍵提示色動態調整
    void MarkShift();

    void configUiPtr();

    void timerUpdate();

    void on_MagephoneButton_01_clicked();
    void on_MagephoneButton_02_clicked();
    void on_MagephoneButton_03_clicked();
    void on_ShipInButton_01_clicked();
    void on_ShipInButton_02_clicked();
    void on_ShipInButton_03_clicked();
    void on_ShipInButton_04_clicked();
    void on_ShipInButton_05_clicked();
    void on_ShipInButton_06_clicked();
    void on_ShipInButton_07_clicked();
    void on_ShipInButton_08_clicked();
    void on_ShipInButton_09_clicked();
    void on_ShipOutButton_01_clicked();
    void on_ShipOutButton_02_clicked();
    void on_ShipOutButton_03_clicked();
    void on_ShipOutButton_04_clicked();
    void on_ShipOutButton_05_clicked();
    void on_ShipOutButton_06_clicked();
    void on_ShipOutButton_07_clicked();
    void on_ShipOutButton_08_clicked();
    void on_ShipOutButton_09_clicked();
    void on_ShipOutButton_10_clicked();
    void on_ShipOutButton_11_clicked();
    void on_ShipOutButton_12_clicked();
    void on_ShipOutButton_13_clicked();
    void on_ShipOutButton_14_clicked();
    void on_ShipOutButton_15_clicked();
    void on_ShipOutButton_16_clicked();
    void on_ShipOutButton_17_clicked();
    void on_ShipOutButton_18_clicked();
    void on_L_speaker_switch_clicked();
    void on_R_speaker_switch_clicked();
    void on_T_speaker_switch_clicked();
    void on_L_mic_switch_clicked();
    void on_R_mic_switch_clicked();
    void on_T_mic_switch_clicked();
    void on_PTTButton_clicked();
    void on_L_HeadSlider_valueChanged(int value);
    void on_R_HeadSlider_valueChanged(int value);
    void on_TableSlider_valueChanged(int value);

    void on_PTTButton_pressed();

    void on_PTTButton_released();

    void on_systemButten_clicked();

private:
    Ui::Dialog *ui;

    //智文a網路模組第一版
    CPOUT sendData;
    CPIN_CMD recvCMD;
    CPIN_Date recvData;
    CPIN_Name recvName;
    QUdpSocket *udpSocket;
    QString LocalIP;
    QString HostIP;
    QString stand_ID;
    int istand_ID;
    int portout;
    int portin;
    char buf_to_net[1024];
    char buf_register[1024];
    char buf_from_net[2048];
    //智文a網路模組第一版_END

    QPushButton *MagephoneBtnPtr[3];
    QPushButton * ShipInBtnPtr[9];
    QPushButton * ShipOutBtnPtr[18];

    //通道按鍵選取上色
    QString checkedColor;
    QString uncheckedColor;

    //Color of BtnText
    QString MagephoneBtnText[3];
    QString ShipInBtnText[9];
    QString ShipOutBtnText[18];

    //通道按鍵上的通話權限1：X聽O說 2：O聽O說
    int MagephoneBtnAuthority[3];
    int ShipInBtnAuthority[9];
    int ShipOutBtnAuthority[18];

    //語音裝備狀態標籤字
    QString SpeakerEnableStr;
    QString SpeakerDisableStr;
    QString MicEnableStr;
    QString MicDisableStr;
    QString MicRestrictionStr;

    //語音裝備狀態標籤字格式
    QFont EnableFont;
    QFont DisableFont;

    //語音裝備狀態標籤字顏色
    QPalette EnableColor;
    QPalette DisableColor;
    QPalette RestrictionColor;


protected:
    void paintEvent(QPaintEvent *);

};

#endif // DIALOG_H
