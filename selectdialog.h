﻿#ifndef SELECTDIALOG_H
#define SELECTDIALOG_H

#include <QDialog>
#include "iomap.h"

namespace Ui {
class SelectDialog;
}

class SelectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SelectDialog(QWidget *parent = 0);
    ~SelectDialog();

private slots:


    void on_LeftChButton_clicked();

    void on_RightChButton_clicked();

    void on_TableChButton_clicked();

private:
    Ui::SelectDialog *ui;
};

#endif // SELECTDIALOG_H
